const express = require('express');
const router = express.Router();
const fs = require('fs');


let messages = [];



router.get('/', (req, res) => {
  const path = "./messages";

  fs.readdir(path, (err, files) => {
    let arrayFiles = files.slice(files.length - 5, files.length);
    console.log(arrayFiles);
    arrayFiles.forEach(file => {
      fs.readFile((path + '/' + file), (err, data) => {

        if (err) {
          console.error(err);
        }
        messages.push(JSON.parse(data));
      });
    });
  });
  let arr = messages;
  messages = [];
  return res.send(arr);
});

router.post('/', (req, res) => {

  const message = {
    text: req.body.text,
    datetime: new Date()
  };

  const file = `./messages/ ${new Date()}`;

  fs.writeFile(file, JSON.stringify(message), (err) => {
    if (err) {
      console.error(err);
    } else {
      console.log('File was saved!');
    }
  });

  return res.send(message);
});

module.exports = router;